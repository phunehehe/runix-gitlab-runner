# [Gitlab Runner](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner) for [Runix](https://gitlab.com/phunehehe/runix)

[![build status](https://gitlab.com/phunehehe/runix-gitlab-runner/badges/master/build.svg)](https://gitlab.com/phunehehe/runix-gitlab-runner/commits/master)

## Dependencies

  - [docker-daemon](https://gitlab.com/phunehehe/runix-docker-daemon)