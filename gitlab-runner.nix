{ runCommand, fetchurl }:
with builtins;
let
  version = "1.5.3";
  src = fetchurl {
    url = "https://gitlab-ci-multi-runner-downloads.s3.amazonaws.com/v${version}/binaries/gitlab-ci-multi-runner-linux-amd64";
    sha256 = "0qin8q0j6558x2zy2n9z06fa88sx9h46ilzi3nvhrvv40rp6sciy";
  };
in runCommand (baseNameOf ./.) {} ''
  mkdir --parents $out/bin
  bin=$out/bin/gitlab-ci-multi-runner
  cp ${src} $bin
  chmod +x $bin
  ln --symbolic $out/bin/gitlab-ci-multi-runner $out/bin/gitlab-runner
''
