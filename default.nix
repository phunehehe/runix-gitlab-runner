{ config, exports, home, lib, pkgs, ... }:
let

  inherit (lib) concatMapStringsSep mkOption;
  inherit (lib.types) listOf str;

  gitlab-runner = pkgs.callPackage ./gitlab-runner.nix {};
  builds-dir = "${home}/builds";
  cache-dir = "${home}/cache";
  name = baseNameOf ./.;
  user = name;

  volumes = [
    cache-dir
    "/cache"
    "/nix:/nix"
  ];

  config-file = pkgs.writeText "config.toml" ''
    [[runners]]
      url = "${config.url}"
      token = "${config.token}"
      builds_dir = "${builds-dir}"
      cache_dir = "${cache-dir}"
      executor = "docker"
      [runners.docker]
        cache_dir = "${cache-dir}"
        volumes = [${concatMapStringsSep "," (v: "\"${v}\"") volumes}]
  '';

in {
  exports.docker-daemon.users = [user];
  options = {
    token = mkOption { type = str; };
    url = mkOption {
      type = str;
      default = https://gitlab.com/ci;
    };
  };

  run = pkgs.writeBash name ''

    PATH=${lib.makeBinPath [
      pkgs.coreutils
    ]}

    id ${user} || ${pkgs.shadow}/bin/useradd --system \
      --home /var/empty \
      ${user}

    for d in ${builds-dir} ${cache-dir}
    do
      mkdir --parents $d
      chown ${user} $d
    done

    exec ${pkgs.runit}/bin/chpst -u ${user}:${exports.docker-daemon.group} \
      ${gitlab-runner}/bin/gitlab-runner run \
        --config ${config-file} \
        --working-directory ${home}
  '';
}
